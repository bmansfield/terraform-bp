# Terraform Boilerplate

A Hashicorp `terraform` boilerplate repo


## About

This is by no means intended on being complete or a working example. It is
intended on being something I can quickly copy and get most of the core pieces
to the infrastructure for a new project, then rip out what I don't need, and
modify the pieces I need to fit the project I'm working on.


## Future Improvements

* RDS
* DynamoDB

