
################################################################################
# Bastion
################################################################################

module "bastion" {
  source = "./modules/bastion"

  create = var.ec2_bastion_create

  depends_on  = [module.network]

  name                        = var.ec2_bastion_name
  ami                         = var.ec2_bastion_ami
  instance_type               = var.ec2_bastion_instance_type
  availability_zone           = try(var.ec2_bastion_availability_zone, element(module.network.azs, 0))
  subnet_id                   = try(var.ec2_bastion_vpc_subnet_id, element(module.network.public_subnets, 0))
  key_name                    = var.ec2_bastion_key_name
  monitoring                  = var.ec2_bastion_monitoring
  hibernation                 = var.ec2_bastion_hibernation
  vpc_security_group_ids      = [module.ssh_sg.maxar_id, module.ssh_sg.pe_id, module.ssh_sg.niwc_id, module.ssh_sg.bah_id]
  iam_instance_profile        = var.ec2_bastion_iam_instance_profile
  associate_public_ip_address = var.ec2_bastion_associate_public_ip_address

  user_data = var.ec2_bastion_user_data_file

  ebs_optimized     = var.ec2_bastion_ebs_optimized
  root_block_device = var.ec2_bastion_root_block_device

  tags = merge(
    var.tags,
    var.ec2_bastion_tags
  )
}

