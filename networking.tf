
################################################################################
# VPC
################################################################################

module "network" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.0"

  create = var.vpc_create

  name = var.vpc_name
  azs  = var.vpc_azs

  cidr            = var.vpc_cidr
  public_subnets  = var.vpc_public_cidrs
  private_subnets = var.vpc_private_cidrs

  create_igw         = var.vpc_create_igw
  enable_nat_gateway = var.vpc_enable_nat_gateway
  enable_vpn_gateway = var.vpc_enable_vpn_gateway

  public_dedicated_network_acl = var.vpc_public_dedicated_network_acl
  public_inbound_acl_rules     = var.vpc_public_inbound_acl
  public_outbound_acl_rules    = var.vpc_public_outbound_acl
  private_inbound_acl_rules    = var.vpc_private_inbound_acl
  private_outbound_acl_rules   = var.vpc_private_outbound_acl

  tags = merge(
    var.tags,
    var.vpc_tags
  )
}

