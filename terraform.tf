terraform {
  required_version = "~> 0.14"

  backend "s3" {
    bucket  = "terraform-state"
    key     = "project.tfstate"
    region  = "us-west-1"
    encrypt = true
  }

  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.57"
    }
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket  = var.s3_tf_state_bucket_name

  depends_on = [module.logging_bucket]

  lifecycle {
    prevent_destroy = true
  }

  versioning {
    enabled = true
  }

  logging {
    target_bucket = var.s3_logging_bucket_name
    target_prefix = var.s3_tf_logging_folder
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

provider "aws" {
  region = var.region
}
