BUILD_DATE := $(strip $(shell date -u +"%Y-%m-%dT%H:%M:%SZ"))
TF_WORKSPACE := development


.PHONY: all
all: plan apply

default: plan

.PHONY: init
init:
	terraform init
	terraform workspace new $(TF_WORKSPACE) || true
	terraform workspace select $(TF_WORKSPACE)

.PHONY: reinit
reinit:
	terraform init -reconfigure

.PHONY: plan
plan:
	terraform plan \
		-var-file=$(TF_WORKSPACE).tfvars \
		-out $(TF_WORKSPACE).out

.PHONY: apply
apply:
	terraform apply $(TF_WORKSPACE).out

.PHONY: install
install: plan apply

.PHONY: refresh
refresh:
	terraform refresh \
		-var-file=$(TF_WORKSPACE).tfvars

.PHONY: rm del delete destroy
rm del delete destroy:
	terraform destroy \
		-refresh=true \
		-var-file=$(TF_WORKSPACE).tfvars

.PHONY: ls list
ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

