
################################################################################
# Security Groups
################################################################################


################################################################################
# VPC Subnets Security Groups
################################################################################

module "sg_public" {
  source = "terraform-aws-modules/security-group/aws"
  version = "4.8.0"

  depends_on = [module.network]

  create = var.sg_public_create

  name        = var.sg_public_name
  description = var.sg_public_description
  vpc_id      = module.network.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port = 0
      to_port = 0
      protocol = "-1"
      description = "Public Subnet"
      cidr_blocks = module.network.public_subnets_cidr_blocks
    }
  ]

  tags = merge(
    var.tags,
    var.sg_public_tags
  )
}

module "sg_private" {
  source = "terraform-aws-modules/security-group/aws"
  version = "4.8.0"

  depends_on = [module.network]

  create = var.sg_private_create

  name        = var.sg_private_name
  description = var.sg_private_description
  vpc_id      = module.network.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port = 0
      to_port = 0
      protocol = "-1"
      description = "Private Subnet"
      cidr_blocks = module.network.private_subnets_cidr_blocks
    }
  ]

  tags = merge(
    var.tags,
    var.sg_private_tags
  )
}


################################################################################
# SSH Security Groups
################################################################################

module "sg_ssh" {
  source = "terraform-aws-modules/security-group/aws"
  version = "4.8.0"

  depends_on = [module.network]

  create = var.sg_ssh_create

  name        = var.sg_ssh_name
  description = var.sg_ssh_description
  vpc_id      = module.network.vpc_id

  ingress_with_cidr_blocks  = var.sg_ssh_ingress_rules
  egress_with_cidr_blocks   = var.sg_ssh_egress_rules

  tags = merge(
    var.tags,
    var.sg_ssh_tags
  )
}

